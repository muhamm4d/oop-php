//Setup
var contacts = [
    {
        "firstName": "Akira",
        "lastName": "Laine",
        "number": "0543236543",
        "likes": ["Pizza", "Coding", "Brownie Points"]
    },
    {
        "firstName": "Harry",
        "lastName": "Potter",
        "number": "0994372684",
        "likes": ["Hogwarts", "Magic", "Hagrid"]
    },
    {
        "firstName": "Sherlock",
        "lastName": "Holmes",
        "number": "0487345643",
        "likes": ["Intriguing Cases", "Violin"]
    },
    {
        "firstName": "Kristian",
        "lastName": "Vos",
        "number": "unknown",
        "likes": ["Javascript", "Gaming", "Foxes"]
    }
];


function lookUpProfile(firstName, prop){
// Only change code below this line
  var temp = 0;
    var propt = prop;
  while (temp < contacts.length) {
      if (contacts[temp].firstName === firstName && contacts[temp].hasOwnProperty(prop)) {
          var property = prop.toString();
          console.log(contacts[temp].property);
      } else {
          console.log("No such property");
      }
      temp++;
  }
// Only change code above this line
}

// Change these values to test your function
lookUpProfile("Akira", "likes");
/*lookUpProfile("Jombi", "likes");
lookUpProfile("Sherlock", "likes");*/