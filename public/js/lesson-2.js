// JS Document - Learn AJAX
$(document).ready(function(){
    
    var xmlHttpReq = new XMLHttpRequest();
    
    xmlHttpReq.open('GET', 'data.json', true);  // open(GET/POST, pathtoJSONFile, async/not) - set parameters
    xmlHttpReq.responseType = 'text';   // making sure the data comming in is a text
    
    xmlHttpReq.onreadystatechange = function() {
        console.log(xmlHttpReq.readyState);
        console.log(xmlHttpReq.status);
        console.log(xmlHttpReq.statusText);
    } // end function -> Not needed

    xmlHttpReq.onload = function() {
        if (xmlHttpReq.status === 200) {
            var userData = JSON.parse(xmlHttpReq.responseText);
            console.log(userData);
        } 
        if (xmlHttpReq.status === 404){
            console.log("Make sure the data is passed");
        }
    } // end onload function
    
    xmlHttpReq.send();  // send() -> activates request()
});