<?php

use Acme\Staff;
use Acme\Business;
use Acme\users\Person;

$muhammad = new Person('Muhammad Mwinchande');
$staff = new Staff([$muhammad]);
$ranatech = new Business($staff);

// hire another staff
$ranatech->hire(new Person('Jane Doe'));

// var_dump($staff);
var_dump($ranatech->getStaffMembers());