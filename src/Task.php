<?php

class Task
{
	protected $task_title;
	protected $task_desc;
	protected $task_handler;
	protected $task_due;
    public $task_completed = false;

    /*
    when you never want to extend the variables in this class
    use private, 
    else use protected.
    else if you want to allow anyone to access these values use
    public
    */

	/**
     * initiate all values.
     *
     * @return mixed
     */
	public function __construct($task_title, $task_desc, $task_handler, $task_due)
	{
		$this->task_title = $task_title;
		$this->task_desc = $task_desc;
		$this->task_handler = $task_handler;
		$this->task_due = $task_due;
	}

    public function complete()
    {
        $this->task_completed = true;
    }

    /**
     * Gets the value of task_title.
     *
     * @return mixed
     */
    public function getTaskTitle()
    {
        return $this->task_title;
    }

    /**
     * Sets the value of task_title.
     *
     * @param mixed $task_title the task title
     *
     * @return self
     */
    protected function setTaskTitle($task_title)
    {
        $this->task_title = $task_title;

        return $this;
    }

    /**
     * Gets the value of task_desc.
     *
     * @return mixed
     */
    public function getTaskDesc()
    {
        return $this->task_desc;
    }

    /**
     * Sets the value of task_desc.
     *
     * @param mixed $task_desc the task desc
     *
     * @return self
     */
    protected function setTaskDesc($task_desc)
    {
        $this->task_desc = $task_desc;

        return $this;
    }

    /**
     * Gets the value of task_handler.
     *
     * @return mixed
     */
    public function getTaskHandler()
    {
        return $this->task_handler;
    }

    /**
     * Sets the value of task_handler.
     *
     * @param mixed $task_handler the task handler
     *
     * @return self
     */
    protected function setTaskHandler($task_handler)
    {
        $this->task_handler = $task_handler;

        return $this;
    }

    /**
     * Gets the value of task_due.
     *
     * @return mixed
     */
    public function getTaskDue()
    {
        return $this->task_due;
    }

    /**
     * Sets the value of task_due.
     *
     * @param mixed $task_due the task due
     *
     * @return self
     */
    protected function setTaskDue($task_due)
    {
        $this->task_due = $task_due;

        return $this;
    }
}

$task = new Task(
    'Learn OOP',
    'OOP is fun to learn',
    'Muhammad',
    '23-May-2017 08:30am'
);

//var_dump($task);

$task->task_completed = true;

var_dump($task);